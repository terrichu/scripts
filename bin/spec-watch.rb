#!/usr/bin/env ruby
# frozen_string_literal: true

require 'digest'
require 'optparse'
require 'set'
require 'benchmark'

SCRIPT_NAME = File.basename($PROGRAM_NAME)

def require_gem!(gem)
  require gem
rescue LoadError
  puts "Couldn't find gem '#{gem}'. Please install it with `gem install #{gem}`"
  exit 1
end

require_gem! 'colorize'

def parse_options
  options = {
    cmd: 'spec-changed.rb',
    delay: 5,
    exit_after: 10 * 60,
    postpone: false,
    clear: true
  }

  OptionParser.new do |opts|
    opts.banner = "Usage: #{SCRIPT_NAME} [options]"

    opts.on('-v', '--debug', 'Enable debug mode') do |v|
      options[:debug] = v
    end
    opts.on('-p', '--postpone', 'Postpone execution') do |v|
      options[:postpone] = v
    end
    opts.on('--clear', 'Clear the terminal before execution') do |v|
      options[:clear] = v
    end
    opts.on('-c', '--cmd=CMD', 'Command to execute') do |v|
      options[:cmd] = v
    end
    opts.on('-d', '--delay=DELAY', Float, 'How many seconds sleep between iterations') do |v|
      options[:delay] = v
    end
    opts.on('-e', '--exit-after=SECONDS', 'Exit after no changes for X seconds') do |v|
      options[:exit_after] = v
    end
  end.parse!

  options
end

def get_sha(files)
  existing_files = files.select { |path| File.exist?(path) }
  content = existing_files.sort.map { |file| File.read(file) }.join("\n")

  Digest::SHA1.hexdigest(content)
end

def get_current_sha
  # changed_files = `git diff --name-only --diff-filter=d $(git merge-base origin/master HEAD)..HEAD`.split("\n").to_set
  # changed_files += `git diff --diff-filter=d --merge-base --name-only origin/master`.split("\n")
  changed_files = `git diff --name-only`.split("\n")

  get_sha(changed_files)
end

def clear
  system("clear") || system("cls")
end

def execute
  options = parse_options
  puts "Options: #{options.inspect}" if options[:debug]

  current_sha = get_current_sha if options[:postpone]
  puts "SHA: #{current_sha}" if options[:debug]

  no_changes_counter = 0

  while true
    new_sha = get_current_sha
    puts "New SHA: #{new_sha}" if options[:debug]

    if new_sha == current_sha
      no_changes_counter += 1
      puts "No changes for #{no_changes_counter} iterations".colorize(:yellow) if options[:debug]

      if no_changes_counter > (options[:exit_after] / options[:delay])
        puts "No changes for #{options[:exit_after]} seconds, exiting".colorize(:red)
        exit
      end

      sleep options[:delay]
      next
    end

    no_changes_counter = 0
    current_sha = new_sha

    clear if options[:clear]
    puts "[Running: #{options[:cmd]}]".colorize(:green)

    system options[:cmd]
    if $?.success?
      puts "[Command was successful]".colorize(:green)
    else
      puts "[Command exited with #{$?.exitstatus}]".colorize(:red)
    end
  end
end

execute