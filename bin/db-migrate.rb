#!/usr/bin/env ruby
# frozen_string_literal: true

require 'optparse'
require 'set'

SCRIPT_NAME = File.basename($PROGRAM_NAME)
MIGRATIONS_DIR = 'db/migrate'

def require_commands!(*commands)
  missing_commands = commands.reject { |command| system("command -v #{command} >/dev/null 2>&1") }

  abort("This script requires #{missing_commands.join(', ')} to be installed.") unless missing_commands.empty?
end

def parse_options
  options = {
    task: :up,
  }
  OptionParser.new do |opts|
    opts.banner = "Usage: #{SCRIPT_NAME} [options]"

    opts.on('--debug', 'Enable debug mode') do |v|
      options[:debug] = v
    end
    opts.on('-t', '--task=TASK', 'Set task') do |v|
      options[:task] = v
    end
  end.parse!

  options
end

def prompt(list, prompt:, multi: false, reverse: false)
  arr = list.join("\n")

  fzf_args = [].tap do |args|
    args << '--multi' if multi
    args << '--tac' if reverse
  end

  output = IO.popen("echo \"#{arr}\" | fzf #{fzf_args.join(' ')} --prompt=\"#{prompt}\"", &:readlines)
  return [] unless output

  selection = output.join.strip

  return selection unless multi

  selection.split("\n")
end

def get_changed_files
  set = `git diff --name-only --diff-filter=d $(git merge-base origin/master HEAD)..HEAD #{MIGRATIONS_DIR}`.split("\n").to_set
  set += `git diff --diff-filter=d --merge-base --name-only origin/master #{MIGRATIONS_DIR}`.split("\n")

  set
end

def execute
  options = parse_options
  puts "Options: #{options.inspect}" if options[:debug]

  files = get_changed_files
  puts "Files: #{files.inspect}" if options[:debug]

  base_files = files.map { |path| File.basename(path) }
  puts "Base files: #{base_files.inspect}" if options[:debug]

  selected_files = prompt(base_files, prompt: 'Select files', multi: true)
  puts "Selected files: #{selected_files.inspect}" if options[:debug]

  if selected_files.empty?
    puts 'No files selected'
    exit 1
  end

  sorted = selected_files.sort_by { |f| f.match(/^\d+/)[0].to_i }
  puts "Sorted: #{sorted.inspect}" if options[:debug]

  case options[:task].to_sym
  when :up
    sorted.each do |file|
      version = file.match(/^\d+/)[0].to_i

      cmd = "bin/rails db:migrate:up:main db:migrate:up:ci VERSION=#{version}"
      puts "$ #{cmd}"
      return unless system(cmd)
    end
  when :down
    sorted.reverse.each do |file|
      version = file.match(/^\d+/)[0].to_i
      cmd = "bin/rails db:migrate:down:main db:migrate:down:ci VERSION=#{version}"
      puts "$ #{cmd}"
      return unless system(cmd)
    end
  end
end

require_commands!('fzf')

execute