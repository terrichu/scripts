#!/usr/bin/env ruby
# frozen_string_literal: true

require 'io/console'
require 'optparse'
require 'yaml'

SCRIPT_NAME = File.basename($PROGRAM_NAME)
CONFIG_NAME = "#{SCRIPT_NAME}.yml".freeze
CONFIG_PATH = File.join(Dir.home, '.config', CONFIG_NAME)

abort("Please create #{CONFIG_PATH}") unless File.exist?(CONFIG_PATH)

TYPE_LABELS = {
  'type::bug' => %w[bug::availability bug::functional bug::performance bug::transient bug::vulnerability],
  'type::feature' => %w[feature::addition feature::enhancement],
  'type::maintenance' => %w[maintenance::dependency maintenance::refactor maintenance::workflow],
  'type::ignore' => []
}.freeze

def require_commands!(*commands)
  missing_commands = commands.reject { |command| system("command -v #{command} >/dev/null 2>&1") }

  abort("This script requires #{missing_commands.join(', ')} to be installed.") unless missing_commands.empty?
end

def parse_config
  YAML.load_file(CONFIG_PATH)
end

def parse_options
  options = {
    push: true,
    set_milestone: false
  }
  OptionParser.new do |opts|
    opts.banner = 'Usage: mr.rb [options]'

    opts.on('--debug', 'Enable debug mode') do |v|
      options[:debug] = v
    end
    opts.on('-e', '--edit-config', 'Edit config file') do |v|
      options[:edit_config] = v
    end
    opts.on('--dry-run', 'Perform dry run') do |v|
      options[:dry_run] = v
    end
    opts.on('-p', '--push', 'Push committed changes') do |v|
      options[:push] = v
    end
    opts.on('-t', '--title', 'Set MR title') do |v|
      options[:title] = v
    end
    opts.on('-d', '--description', 'Set MR description') do |v|
      options[:description] = v
    end
    opts.on('-m', '--[no]-milestone', 'Set MR milestone from config file') do |v|
      options[:set_milestone] = v
    end
  end.parse!

  options
end

def prompt(list, prompt:, multi: false, reverse: false)
  arr = list.join("\n")

  fzf_args = [].tap do |args|
    args << '--multi' if multi
    args << '--tac' if reverse
  end

  output = IO.popen("echo \"#{arr}\" | fzf #{fzf_args.join(' ')} --prompt=\"#{prompt}\"", &:readlines)
  return [] unless output

  selection = output.join.strip

  return selection unless multi

  selection.split("\n")
end

def update_issue_in_review
  current_branch = `git current-branch`.strip
  match = current_branch.match(/(?<iid>\d+)-.+/)
  return false unless match

  `glab issue update #{match[:iid]} -l "workflow::in review"`
end

def execute
  options = parse_options
  exec "$EDITOR #{CONFIG_PATH}" if options[:edit_config]

  puts "> Options: #{options.inspect}" if options[:debug]

  config = parse_config

  puts "> Config: #{config.inspect}" if options[:debug]

  mr_exists = nil
  mr_exists_thread = Thread.new { mr_exists = system('glab mr view &> /dev/null') }

  mr_labels = []
  mr_labels << config['labels']

  type_label = prompt(TYPE_LABELS.keys, prompt: 'Select type >')
  unless type_label.empty?
    mr_labels << type_label

    subtype_label = prompt(TYPE_LABELS[type_label], prompt: 'Select subtype >')
    mr_labels << subtype_label unless subtype_label.empty?
  end

  reviewer = prompt(config['reviewers'], prompt: 'Select reviewer >') unless config['reviewers'].empty?

  cmd = []

  mr_exists_thread.join
  if mr_exists
    cmd << %w[glab mr update]
    cmd << config.dig('glab_arguments', 'update')
  else
    cmd << %w[glab mr create]
    cmd << config.dig('glab_arguments', 'create')
  end
  cmd << config.dig('glab_arguments', 'common')

  if reviewer.nil? || reviewer.empty?
    cmd << '--draft'
    mr_labels << 'workflow::in dev'
  else
    cmd << %(--reviewer "#{reviewer}")
    mr_labels << 'workflow::in review'
  end

  cmd << %(--label "#{mr_labels.flatten.compact.join(',')}")
  cmd << ARGV.join(' ') unless ARGV.empty?
  cmd << %(-t "#{options[:title]}") unless options[:title].nil? || options[:title].empty?
  cmd << %(-d "#{options[:description]}") unless options[:description].nil? || options[:description].empty?
  cmd << '--push' if options[:push] && !mr_exists
  cmd << %(--milestone "#{config['milestone']}") if options[:set_milestone]

  exec_cmd = cmd.flatten.compact.join(' ')

  if options[:dry_run]
    puts exec_cmd
    return
  end

  puts 'Continue with MR operation (y/n): '
  char = $stdin.getch.downcase
  unless char == 'y'
    puts 'Script aborted.'
    exit 1
  end

  if mr_labels.include?('workflow::in review')
    puts '> Updating issue label'
    update_issue_in_review
  end

  puts "$ #{exec_cmd}"
  exec exec_cmd
end

require_commands!('fzf', 'glab')

execute
