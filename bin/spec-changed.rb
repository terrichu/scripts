#!/usr/bin/env ruby
# frozen_string_literal: true

require 'set'
require 'colorize'

SCRIPT_NAME = File.basename($PROGRAM_NAME)

def get_files
  files = `git diff --name-only --diff-filter=d $(git merge-base origin/master HEAD)..HEAD`.split("\n").to_set
  files += `git diff --diff-filter=d --merge-base --name-only origin/master`.split("\n")
  files += `git diff --name-only`.split("\n")

  files.select { |path| File.exist?(path) && path.end_with?('_spec.rb') }
end

def execute
  files = get_files

  if files.empty?
    puts "There are no spec files. Aborting".colorize(:red)
    exit 1
  end

  arr = []
  arr << "bin/rspec #{files.join(' ')}"
  arr << ARGV.join(' ') if ARGV.any?

  cmd = arr.join(' ')
  puts "$ #{cmd}"
  exec cmd
end

execute