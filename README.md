# scripts

This is a repository with useful scripts I've developed to increase my productivity.

## How to set up

1. Clone this repository into your projects folder (in this instruction I assume it's `$HOME/projects`)
    ```shell
    cd $HOME/projects
    git clone git@gitlab.com:dgruzd/scripts.git dgruzd-scripts
    ```
1. Add these lines to your rc file (for example, `~/.zshrc`)
    ```shell
    DGRUZD_SCRIPTS="$HOME/projects/dgruzd-scripts"
    if [ -d "$DGRUZD_SCRIPTS" ]; then
        export PATH="$DGRUZD_SCRIPTS/bin:$PATH"
    fi
    ```
1. Now you can execute the commands from the `bin` directory. You can reload the terminal and verify that it works with
   ```shell
   brew-install
   ```

## How to upgrade
1. Execute git pull in the project directory
   ```shell
   cd $DGRUZD_SCRIPTS
   git pull
   ```

## List of scripts

| Script    | Description |
| -------- | ------- |
| [`brew-install`](./bin/brew-install)    | It is an fzf interface to `brew install`. It caches the list of apps for 1 week to make execution fast. |
| [`brew-uninstall`](./bin/brew-uninstall)    | It is an fzf interface to `brew uninstall`. |
| [`vimf`](./bin/vimf) | It is a fuzzy finder to quickly filter and open files in vim. |
| [`mr.rb`](./bin/mr.rb) | It is a configurable wrapper for mr creation/updates that uses [`glab`](https://gitlab.com/gitlab-org/cli) under the hood. |
| [`spec-watch.rb`](./bin/spec-watch.rb) | This is a script to monitor changes in a git repository and execute a specified command. |
| [`spec-changed.rb`](./bin/spec-changed.rb) | This is a script to run rspec for all changed spec files in the current branch. |

## Demos

- [`brew-install`](https://asciinema.org/a/Jahd7JhKqOv1l4K1TMFL6tgwy)
- [`brew-uninstall`](https://asciinema.org/a/zOBMm7dOZ8cNAJ3retnD0tVxP)
- [`vimf`](https://asciinema.org/a/zBn3hHhnMursCqX2Y33qb33mx)